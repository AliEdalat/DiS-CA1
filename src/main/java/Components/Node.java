package Components;
import Events.AbsorbMessage;
import Events.AcceptMessage;
import Events.AddLwoeMessage;
import Events.InitMessage;
import Events.LwoeMessage;
import Events.MergeMessage;
import Events.RejectMessage;
import Events.ReportMessage;
import Events.TestMessage;
import Events.TryMessage;
import Events.UpdateRootMessage;
import Ports.EdgePort;
import misc.TableRow;
import se.sics.kompics.*;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.WRITE;


public class Node extends ComponentDefinition {
    Positive<EdgePort> recievePort = positive(EdgePort.class);
    Negative<EdgePort> sendPort = negative(EdgePort.class);
    Boolean isRoot = false;
    public String nodeName;
    public String parentName = "";
    ArrayList<String> children = new ArrayList<String>();
    ArrayList<TestMessage> waitedTests = new ArrayList<TestMessage>();
    Set<String> tests;
    ArrayList<String> lwoeSrcs = new ArrayList<String>();
    ArrayList<String> reporters = new ArrayList<String>();
    int fragmentName = 0;
    int level = 0;
    boolean findLwoe = false;
    int lwoe = Integer.MAX_VALUE;
    int dist = 10000;
    String dstMerge;
    String report = "";

    HashMap<String,Integer> neighbours = new HashMap<>();
    ArrayList<TableRow> route_table = new ArrayList<>();

    Handler mergeHandler = new Handler<MergeMessage>(){
        @Override
        public void handle(MergeMessage event) {
            if (nodeName.equalsIgnoreCase(event.dst)){
               // System.out.println(nodeName +  " recieved message : src " + event.src + " dst " + event.dst);
                if(level > event.level) {
                	if(findLwoe == false) {
                		//System.out.println("abs");
                		children.add(event.src);
//                		if(!parentName.equals("")) {
//                        	trigger(new AbsorbMessage(nodeName, parentName),sendPort);
//                        } else {
//                        	for(String child : children) {
//                    			System.out.println("\t" + child);
                    			trigger(new UpdateRootMessage(nodeName, event.src, level, fragmentName),sendPort);
//                    		}
//                        }
                	}
                	return;
                }
                if(dstMerge.equalsIgnoreCase(event.src) && level == event.level) {
                	if (event.dst.compareToIgnoreCase(event.src) < 0 && !isChild(event.src)) {
                		level++;
                		answerWaitedTests();
                		fragmentName = findWeightTo(event.src);
                		lwoe = Integer.MAX_VALUE;
                		//System.out.println(nodeName + " is root " + level + " " + fragmentName);
                		if (!parentName.equalsIgnoreCase(""))
                			children.add(parentName);
                		children.add(event.src);
                		//System.out.println(children.size());
                		parentName = "";
//                		tests =  neighbours.keySet();
                		tests = new HashSet<String>();
                		for ( String key : neighbours.keySet() ) {
                		    tests.add(key);
                		}
                		tests.removeAll(children);
                		tests.remove(parentName);
                		for(String neighbour : tests){
                			trigger(new TestMessage(fragmentName, level, neighbour, nodeName),sendPort);
                		}
                		for(String child : children) {
//                			System.out.println("\t" + child);
                			trigger(new UpdateRootMessage(nodeName, child, level, fragmentName),sendPort);
                		}
					}
                	return;
                }
    			trigger(new TryMessage(nodeName, event.src),sendPort);
            }
        }
    };
    
    private boolean isChild(String src) {
    	for(String i : children) {
    		if(i.equals(src)) {
    			return true;
    		}
    	}
    	return false;
    }
    
    Handler tryHandler = new Handler<TryMessage>(){
        @Override
        public void handle(TryMessage event) {
            if (nodeName.equalsIgnoreCase(event.dst)){
                //System.out.println("try message : src " + event.src + " dst " + event.dst);
                dstMerge = event.src;
                trigger(new MergeMessage(nodeName, event.src, level, fragmentName),sendPort);
            }
        }
    };
    
    Handler absorbHandler = new Handler<AbsorbMessage>(){
        @Override
        public void handle(AbsorbMessage event) {
            if (nodeName.equalsIgnoreCase(event.dst)){
                //System.out.println("absorb message : src " + event.src + " dst " + event.dst);
                if(!parentName.equals("")) {
                	trigger(new AbsorbMessage(nodeName, parentName),sendPort);
                } else {
                	for(String child : children) {
//            			System.out.println("\t" + child);
            			trigger(new UpdateRootMessage(nodeName, child, level, fragmentName),sendPort);
            		}
                }
            }
        }
    };
    
    Handler updateRootHandler = new Handler<UpdateRootMessage>(){
        @Override
        public void handle(UpdateRootMessage event) {
            if (nodeName.equalsIgnoreCase(event.dst) && event.level > level){
                //System.out.println(nodeName +  " recieved updateRootmessage : src " + event.src + " dst " + event.dst);
        		level = event.level;
        		fragmentName = event.fragmentName;
//        		parentName = event.src;
        		//System.out.println("level: " + level);
        		answerWaitedTests();
        		lwoe = Integer.MAX_VALUE;
        		findLwoe = false;
        		lwoeSrcs = new ArrayList<String>();
        		if (!parentName.equals(""))
        			children.add(parentName);
        		children.remove(event.src);
        		//System.out.println("children");
        		for(String ch : children) {
        			//System.out.println("\t" + ch);
        		}
        		parentName = event.src;
        		//System.out.println(parentName);
        		//System.out.println("********");
//        		tests =  neighbours.keySet();
        		tests = new HashSet<String>();
        		for ( String key : neighbours.keySet() ) {
        		    tests.add(key);
        		}
        		tests.removeAll(children);
        		tests.remove(parentName);
        		if(getAllChildrenLwoe() && tests.isEmpty()) {
        			findLwoe = true;
    				//System.out.println(nodeName + " find lwoe " + lwoe + " send to " + parentName);
    				if (!parentName.equals("")) {
    					trigger(new LwoeMessage(nodeName, parentName, lwoe),sendPort);
    				} else {
    					//System.out.println("root " + nodeName + " lwoe " + lwoe);
    					for (Map.Entry mapElement : neighbours.entrySet()) { 
    	                    String key = (String)mapElement.getKey();
    	                    int value = (int)mapElement.getValue(); 
    	                    if(value == lwoe) {
    	                    	dstMerge = key;
    	                    	trigger(new MergeMessage(nodeName, key, level, fragmentName),sendPort);
    	                    	return;
    	                    }
    	                }
    					for(String child : children) {
    						trigger(new AddLwoeMessage(nodeName, child, lwoe),sendPort);
    	        		}
    				}
        		}
        		for(String neighbour : tests){
        			//System.out.println("\t" + neighbour);
        			trigger(new TestMessage(fragmentName, level, neighbour, nodeName),sendPort);
        		}
        		for(String child : children) {
        			trigger(new UpdateRootMessage(nodeName, child, level, fragmentName),sendPort);
        		}
            }
        }
    };
    
    Handler testHandler = new Handler<TestMessage>(){
        @Override
        public void handle(TestMessage event) {
//        	System.out.println("testMessage from " + event.src + " to " + event.dst);
//        	System.out.println(nodeName);
        	if (nodeName.equalsIgnoreCase(event.dst)){
        		//System.out.println(nodeName + " :testMessage from " + event.src + " to " + event.dst);
	        	if(event.fragmentName == fragmentName) {
	        		trigger(new RejectMessage(event.src, nodeName),sendPort);
	        	} else if(event.level <= level) {
	        		trigger(new AcceptMessage(event.src, nodeName),sendPort);
	        	} else {
	        		//System.out.println("then wait until level(j) =level(i) and then send “accept/reject");
	        		waitedTests.add(event);
//	        		trigger(new AcceptMessage(event.src, nodeName),sendPort);
	        	}
        	}
        	
        }
    };
    
    Handler acceptHandler = new Handler<AcceptMessage>(){
        @Override
        public void handle(AcceptMessage event) {
//        	trigger(new RejectMessage(),sendPort);
        	if (nodeName.equalsIgnoreCase(event.dst)){
        		//System.out.println("acceptMessage from " + event.src + " to " + event.dst);
        		if (!tests.isEmpty())
        			tests.remove(event.src);
        		//System.out.println("asd");
        		lwoe = (lwoe > findWeightTo(event.src)) ? findWeightTo(event.src) : lwoe;
        		//System.out.println(lwoe);
        		for(String i : tests) {
        			//System.out.println("\t" + i);
        		}
        		if(tests.isEmpty()) {
        			if(children.isEmpty()) {
        				findLwoe = true;
        				//System.out.println(nodeName + " find lwoe " + lwoe + " send to " + parentName);
        				if (!parentName.equals("")) {
        					trigger(new LwoeMessage(nodeName, parentName, lwoe),sendPort);
        				} else {
        					//System.out.println("root " + nodeName + " lwoe " + lwoe);
        					for (Map.Entry mapElement : neighbours.entrySet()) { 
        	                    String key = (String)mapElement.getKey();
        	                    int value = (int)mapElement.getValue(); 
        	                    if(value == lwoe) {
        	                    	dstMerge = key;
        	                    	trigger(new MergeMessage(nodeName, key, level, fragmentName),sendPort);
        	                    	return;
        	                    }
        	                }
        					for(String child : children) {
        						trigger(new AddLwoeMessage(nodeName, child, lwoe),sendPort);
        	        		}
        				}
        			} else if (getAllChildrenLwoe()) {
        				findLwoe = true;
        				//System.out.println(nodeName + " find lwoe " + lwoe + " send to " + parentName);
        				if (!parentName.equals("")) {
        					trigger(new LwoeMessage(nodeName, parentName, lwoe),sendPort);
        				} else {
        					//System.out.println("root " + nodeName + " lwoe " + lwoe);
        					for (Map.Entry mapElement : neighbours.entrySet()) { 
        	                    String key = (String)mapElement.getKey();
        	                    int value = (int)mapElement.getValue(); 
        	                    if(value == lwoe) {
        	                    	dstMerge = key;
        	                    	trigger(new MergeMessage(nodeName, key, level, fragmentName),sendPort);
        	                    	return;
        	                    }
        	                }
        					for(String child : children) {
        						trigger(new AddLwoeMessage(nodeName, child, lwoe),sendPort);
        	        		}
        				}
        			}
        		} else if (canIgnoreTests() && (getAllChildrenLwoe() || children.isEmpty())) {
        			findLwoe = true;
        			if (!parentName.equals("")) {
    					trigger(new LwoeMessage(nodeName, parentName, lwoe),sendPort);
    				} else {
    					//System.out.println("root " + nodeName + " lwoe " + lwoe);
    					for (Map.Entry mapElement : neighbours.entrySet()) { 
    	                    String key = (String)mapElement.getKey();
    	                    int value = (int)mapElement.getValue(); 
    	                    if(value == lwoe) {
    	                    	dstMerge = key;
    	                    	trigger(new MergeMessage(nodeName, key, level, fragmentName),sendPort);
    	                    	return;
    	                    }
    	                }
    					for(String child : children) {
    						trigger(new AddLwoeMessage(nodeName, child, lwoe),sendPort);
    	        		}
    				}
        		}
        	}
        }
    };
    
    Handler rejectHandler = new Handler<RejectMessage>(){
        @Override
        public void handle(RejectMessage event) {
//        	trigger(new RejectMessage(),sendPort);
        	if (nodeName.equalsIgnoreCase(event.dst)){
        		//System.out.println("rejectMessage from " + event.src + " to " + event.dst);
        		if (!tests.isEmpty())
        			tests.remove(event.src);
        		//System.out.println("asd");
        		if(tests.isEmpty()) {
        			if(children.isEmpty()) {
        				findLwoe = true;
        				//System.out.println(nodeName + " find lwoe " + lwoe + " send to " + parentName);
        				if (!parentName.equals("")) {
        					trigger(new LwoeMessage(nodeName, parentName, lwoe),sendPort);
        				} else {
        					//System.out.println("root " + nodeName + " lwoe " + lwoe);
        					for (Map.Entry mapElement : neighbours.entrySet()) { 
        	                    String key = (String)mapElement.getKey();
        	                    int value = (int)mapElement.getValue(); 
        	                    if(value == lwoe) {
        	                    	dstMerge = key;
        	                    	trigger(new MergeMessage(nodeName, key, level, fragmentName),sendPort);
        	                    	return;
        	                    }
        	                }
        					for(String child : children) {
        						trigger(new AddLwoeMessage(nodeName, child, lwoe),sendPort);
        	        		}
        				}
        			} else if (getAllChildrenLwoe()) {
        				findLwoe = true;
        				//System.out.println(nodeName + " find lwoe " + lwoe + " send to " + parentName);
        				if (!parentName.equals("")) {
        					trigger(new LwoeMessage(nodeName, parentName, lwoe),sendPort);
        				} else {
        					//System.out.println("root " + nodeName + " lwoe " + lwoe);
        					for (Map.Entry mapElement : neighbours.entrySet()) { 
        	                    String key = (String)mapElement.getKey();
        	                    int value = (int)mapElement.getValue(); 
        	                    if(value == lwoe) {
        	                    	dstMerge = key;
        	                    	trigger(new MergeMessage(nodeName, key, level, fragmentName),sendPort);
        	                    	return;
        	                    }
        	                }
        					for(String child : children) {
        						trigger(new AddLwoeMessage(nodeName, child, lwoe),sendPort);
        	        		}
        				}
        			}
        		} else if (canIgnoreTests() && (getAllChildrenLwoe() || children.isEmpty())) {
        			findLwoe = true;
        			if (!parentName.equals("")) {
    					trigger(new LwoeMessage(nodeName, parentName, lwoe),sendPort);
    				} else {
    					//System.out.println("root " + nodeName + " lwoe " + lwoe);
    					for (Map.Entry mapElement : neighbours.entrySet()) { 
    	                    String key = (String)mapElement.getKey();
    	                    int value = (int)mapElement.getValue(); 
    	                    if(value == lwoe) {
    	                    	dstMerge = key;
    	                    	trigger(new MergeMessage(nodeName, key, level, fragmentName),sendPort);
    	                    	return;
    	                    }
    	                }
    					for(String child : children) {
    						trigger(new AddLwoeMessage(nodeName, child, lwoe),sendPort);
    	        		}
    				}
        		}
        	}
        }
    };
    
    Handler lwoeHandler = new Handler<LwoeMessage>(){
        @Override
        public void handle(LwoeMessage event) {
//        	trigger(new RejectMessage(),sendPort);
        	if (nodeName.equalsIgnoreCase(event.dst)){
        		//System.out.println("lwoeMessage from " + event.src + " to " + event.dst + " val " + event.lwoe);
        		for(String i : tests) {
        			//System.out.println("\t" + i);
        		}
        		//System.out.println("&&&&&&&&&&&&&&&&");
        		lwoeSrcs.add(event.src);
        		lwoe = (lwoe > event.lwoe) ? event.lwoe : lwoe;
        		//System.out.println(lwoe);
        		if(getAllChildrenLwoe()) {
	        		if(tests.isEmpty()) {
	        			//System.out.println("ooop");
	        			findLwoe = true;
	        			if (!parentName.equals("")) {
	        				//System.out.println("PPPPPPPPPPPO");
        					trigger(new LwoeMessage(nodeName, parentName, lwoe),sendPort);
        				} else {
        					//System.out.println("root " + nodeName + " lwoe " + lwoe);
        					for (Map.Entry mapElement : neighbours.entrySet()) { 
        	                    String key = (String)mapElement.getKey();
        	                    int value = (int)mapElement.getValue(); 
        	                    if(value == lwoe) {
        	                    	dstMerge = key;
        	                    	trigger(new MergeMessage(nodeName, key, level, fragmentName),sendPort);
        	                    	return;
        	                    }
        	                }
        					for(String child : children) {
        						trigger(new AddLwoeMessage(nodeName, child, lwoe),sendPort);
        	        		}
        				}
	        		} else if (canIgnoreTests()) {
	        			findLwoe = true;
	        			if (!parentName.equals("")) {
        					trigger(new LwoeMessage(nodeName, parentName, lwoe),sendPort);
        				} else {
        					//System.out.println("root " + nodeName + " lwoe " + lwoe);
        					for (Map.Entry mapElement : neighbours.entrySet()) { 
        	                    String key = (String)mapElement.getKey();
        	                    int value = (int)mapElement.getValue(); 
        	                    if(value == lwoe) {
        	                    	dstMerge = key;
        	                    	trigger(new MergeMessage(nodeName, key, level, fragmentName),sendPort);
        	                    	return;
        	                    }
        	                }
        					for(String child : children) {
        						trigger(new AddLwoeMessage(nodeName, child, lwoe),sendPort);
        	        		}
        				}
	        		}
        		}
        	}
        }
    };

    Handler addLwoeHandler = new Handler<AddLwoeMessage>(){
        @Override
        public void handle(AddLwoeMessage event) {
//        	trigger(new RejectMessage(),sendPort);
        	if (nodeName.equalsIgnoreCase(event.dst)){
        		//System.out.println("addLwoeMessage from " + event.src + " to " + event.dst + " val " + event.lwoe);
        		for (Map.Entry mapElement : neighbours.entrySet()) { 
                    String key = (String)mapElement.getKey();
                    int value = (int)mapElement.getValue(); 
                    if(value == event.lwoe) {
                    	dstMerge = key;
                    	trigger(new MergeMessage(nodeName, key, level, fragmentName),sendPort);
                    	return;
                    }
                }
        		if(children.isEmpty() && event.lwoe == Integer.MAX_VALUE) {
        			trigger(new ReportMessage(nodeName, parentName, nodeName + "-" + parentName
        					+ "," + findWeightTo(parentName)), sendPort);
        			return;
        		}
        		for(String child : children) {
					trigger(new AddLwoeMessage(nodeName, child, lwoe),sendPort);
        		}
        	}
        }
    };
    
    private void sendMergeToLwoe() {
    	for (Map.Entry mapElement : neighbours.entrySet()) { 
            String key = (String)mapElement.getKey();
            int value = (int)mapElement.getValue(); 
            if(value == lwoe) {
            	dstMerge = key;
            	trigger(new MergeMessage(nodeName, key, level, fragmentName),sendPort);
            }
        }
    }
    

    Handler reportHandler = new Handler<ReportMessage>() {
        @Override
        public void handle(ReportMessage event) {
            if (nodeName.equalsIgnoreCase(event.dst))
            {
            	if(!parentName.equals("")) {
            		report += event.report + "\n";
            		reporters.add(event.src);
            		if(getAllChildrenReport()) {
            			report += nodeName + "-" + parentName + "," + findWeightTo(parentName);
            			trigger(new ReportMessage(nodeName, parentName, report),sendPort);
            		}
            	} else {
            		report += event.report + "\n";
            		reporters.add(event.src);
            		if(getAllChildrenReport()) {
            			System.out.println(report);
            			try {
							FileWriter fileWriter = new FileWriter("result.txt");
							PrintWriter printWriter = new PrintWriter(fileWriter);
	        			    printWriter.print(report);
	        			    printWriter.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
            		}
            	}
            }
        }
    };
    
    private boolean getAllChildrenReport() {
    	return new HashSet<>(reporters).equals(new HashSet<>(children));
    }



    Handler startHandler = new Handler<Start>() {
        @Override
        public void handle(Start event) {
            dist = 0;
            int minValue = Integer.MAX_VALUE;
            Map.Entry<String, Integer> minEdge = null;
            for( Map.Entry<String, Integer> entry : neighbours.entrySet())
            {
            	if(entry.getValue() < minValue){
            		minEdge = entry;
            		minValue = entry.getValue();
            	}
            }
            dstMerge = minEdge.getKey();
            trigger(new MergeMessage(nodeName, minEdge.getKey(), level, fragmentName),sendPort);
        }
    };

    public Node(InitMessage initMessage) {
        nodeName = initMessage.nodeName;
        //System.out.println("initNode :" + initMessage.nodeName);
        this.neighbours = initMessage.neighbours;
        for (Map.Entry mapElement : neighbours.entrySet()) { 
            String key = (String)mapElement.getKey(); 
            int value = (int)mapElement.getValue(); 
            //System.out.println(key + " : " + value); 
        }
       // System.out.println(children.size());
        this.isRoot = initMessage.isRoot;
        subscribe(startHandler, control);
        subscribe(reportHandler,recievePort);
        subscribe(mergeHandler,recievePort);
        subscribe(testHandler,recievePort);
        subscribe(acceptHandler,recievePort);
        subscribe(rejectHandler,recievePort);
        subscribe(updateRootHandler,recievePort);
        subscribe(lwoeHandler,recievePort);
        subscribe(addLwoeHandler,recievePort);
        subscribe(absorbHandler,recievePort);
        subscribe(tryHandler,recievePort);
    }

    private int findWeightTo(String to) {
//    	System.out.println(to);
    	for (Map.Entry mapElement : neighbours.entrySet()) { 
            String key = (String)mapElement.getKey();
//            System.out.println(key);
            int value = (int)mapElement.getValue(); 
            if(key.equalsIgnoreCase(to)) {
//            	System.out.println(to + " : " + value);
            	return value;
            }
        }
    	return Integer.MAX_VALUE;
    }
    
    private ArrayList<String> updateChildren(String src) {
    	ArrayList<String> res = new ArrayList<String>();
		if (!parentName.equals(""))
			res.add(parentName);
		for(String i : children) {
			if(!i.equalsIgnoreCase(src)) {
				res.add(i);
			}
		}
		return res;
    }
    
    private boolean getAllChildrenLwoe() {
    	return new HashSet<>(lwoeSrcs).equals(new HashSet<>(children));
    }
    
    private void answerWaitedTests() {
    	ArrayList<TestMessage> temp = new ArrayList<TestMessage>();
    	for(TestMessage e : waitedTests) {
    		//System.out.println(e.src + " -> " + e.dst + " : name " + e.fragmentName + " level " + e.level);
    		if (e.fragmentName == fragmentName) {
    			trigger(new RejectMessage(e.src, nodeName),sendPort);
    			temp.add(e);
    			//System.out.println("rej rm " + e.src + "to" + e.dst);
    		} else if(e.level == level) {
        		trigger(new AcceptMessage(e.src, nodeName),sendPort);
        		temp.add(e);
        		//System.out.println("acc rm " + e.src + "to" + e.dst);
        	}
    	}
    	waitedTests.removeAll(temp);
    }
    
    private boolean canIgnoreTests() {
    	for(String i : tests) {
    		if(findWeightTo(i) < lwoe) {
    			return false;
    		}
    	}
    	return true;
    }
}

