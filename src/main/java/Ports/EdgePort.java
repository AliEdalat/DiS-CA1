package Ports;

import Events.AbsorbMessage;
import Events.AcceptMessage;
import Events.AddLwoeMessage;
import Events.LwoeMessage;
import Events.MergeMessage;
import Events.RejectMessage;
import Events.ReportMessage;
import Events.TestMessage;
import Events.TryMessage;
import Events.UpdateRootMessage;
import se.sics.kompics.PortType;

public class EdgePort extends PortType {{
    positive(MergeMessage.class);
    positive(ReportMessage.class);
    positive(TestMessage.class);
    positive(AcceptMessage.class);
    positive(RejectMessage.class);
    positive(UpdateRootMessage.class);
    positive(LwoeMessage.class);
    positive(AddLwoeMessage.class);
    positive(AbsorbMessage.class);
    positive(TryMessage.class);
    
    negative(MergeMessage.class);
    negative(ReportMessage.class);
    negative(TestMessage.class);
    negative(AcceptMessage.class);
    negative(RejectMessage.class);
    negative(UpdateRootMessage.class);
    negative(LwoeMessage.class);
    negative(AddLwoeMessage.class);
    negative(AbsorbMessage.class);
    negative(TryMessage.class);
}}
