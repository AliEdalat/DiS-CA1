package Events;

import se.sics.kompics.KompicsEvent;

public class RejectMessage implements KompicsEvent {
	public String dst;
	public String src;
	
	public RejectMessage(String dst, String src) {
		this.src = src;
		this.dst = dst;
	}
}
