package Events;

import se.sics.kompics.KompicsEvent;

public class AbsorbMessage implements KompicsEvent {
	public String src;
    public String dst;

    public AbsorbMessage(String src, String dst) {
        this.src = src;
        this.dst = dst;
    }
}
