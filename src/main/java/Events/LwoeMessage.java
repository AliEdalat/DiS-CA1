package Events;

import se.sics.kompics.KompicsEvent;

public class LwoeMessage implements KompicsEvent {
	public String src;
    public String dst;
    public int lwoe;

    public LwoeMessage(String src, String dst, int lwoe) {
        this.src = src;
        this.dst = dst;
        this.lwoe = lwoe;
    }
}
