package Events;

import se.sics.kompics.KompicsEvent;

public class UpdateRootMessage implements KompicsEvent {
    public String src;
    public String dst;
    public int level;
    public int fragmentName;
    
    public UpdateRootMessage(String src, String dst, int level, int fragmentName) {
		this.src = src;
		this.dst = dst;
		this.level = level;
		this.fragmentName = fragmentName;
	}

}
