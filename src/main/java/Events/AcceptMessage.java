package Events;

import se.sics.kompics.KompicsEvent;

public class AcceptMessage implements KompicsEvent {
	public String dst;
	public String src;
	
	public AcceptMessage(String dst, String src) {
		this.dst = dst;
		this.src = src;
	}
}
