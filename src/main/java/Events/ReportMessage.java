package Events;


import misc.TableRow;
import se.sics.kompics.KompicsEvent;

import java.util.ArrayList;

public class ReportMessage implements KompicsEvent {

    public String dst;
    public String src;
    public String report;

    public ReportMessage( String src, String dst , String report) {
        this.dst = dst;
        this.src = src;
        this.report = report;
    }
}
