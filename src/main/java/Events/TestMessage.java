package Events;

import se.sics.kompics.KompicsEvent;

public class TestMessage implements KompicsEvent {
	public int fragmentName;
	public int level;
	public String dst;
	public String src;
	
	public TestMessage(int fragmentName, int level, String dst, String src) {
        this.fragmentName = fragmentName;
        this.level = level;
        this.dst = dst;
        this.src = src;
    }
}
