package Events;

import se.sics.kompics.KompicsEvent;

public class AddLwoeMessage implements KompicsEvent {
	public String src;
    public String dst;
    public int lwoe;

    public AddLwoeMessage(String src, String dst, int lwoe) {
        this.src = src;
        this.dst = dst;
        this.lwoe = lwoe;
    }
}
