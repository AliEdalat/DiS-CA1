package Events;

import se.sics.kompics.KompicsEvent;

public class TryMessage implements KompicsEvent {
	public String src;
    public String dst;

    public TryMessage(String src, String dst) {
        this.src = src;
        this.dst = dst;
    }
}
